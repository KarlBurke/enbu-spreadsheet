﻿using eWare;
using CRMDotNet.eWare;
using CRMDotNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMDotNet.Includes;



namespace CRMDotNet.Enbu
{
    public partial class ReportExample : System.Web.UI.Page
    {

        #region pageLoad
        protected void Page_Load(object sender, EventArgs e)
        {

            int OutputType = 3;     // 2 - single datatable to screen     2 - Single DataTable to excel     3 - Report to Screen        4 - Report to Excel   5- single data to screen with previous values

            #region Create DataTables and Field Lists for the ouput

            //Create First Data Table
            DataTable dtCompanyies = new DataTable();
            dtCompanyies = SpreadSheet.GenerateStaticTestData();

            //Create a collection of fields For the 1st Data Table
            SpreadSheet.FieldCollection fcCompanies = new SpreadSheet.FieldCollection();
            fcCompanies.InsertField("ID", "ID", true, false, 10);
            fcCompanies.InsertField("CompanyID", "Company Ref", true, false, 10);
            fcCompanies.InsertField("UserName", "User", true);
            fcCompanies.InsertField("CompanyName", "User", true);

            //Create Second Data Table
            DataTable dtCompanyies2 = new DataTable();
            dtCompanyies2 = SpreadSheet.GenerateStaticTestData();

            //Create a collection of fields For the 1st Data Table
            SpreadSheet.FieldCollection fcCompanies2 = new SpreadSheet.FieldCollection();
            fcCompanies2.InsertField("ID", "ID", true, false, 10);
            fcCompanies2.InsertField("CompanyID", "Company Ref", true, true, 10);
            fcCompanies2.InsertField("UserName", "User", true, true);   // The extra optional true parameter means this file will show a previous value when rendered
            fcCompanies2.InsertField("CompanyName", "Company", true, true);   // Performs the calculations to work out the previous values

            #endregion
            
            #region Create a Report Data Object for each datatable - Set parameters for the datatable here (show headings, previous values, titles etc)

            //Create a Report Data Object and set parameters
            DataBlock edo = DataBlock.createEnbuDataObject(dtCompanyies, fcCompanies);
            edo.titleString = "Company Monthly Report";
            edo.SubTitles.Add("Between Jan 2012 and Feb 2015");
            edo.SubTitles.Add("Status: Active, Closed, OverDue");
            edo.showColumnHeaders = false;
            
            //Create a Report Data Object and set parameters
            DataBlock edo2 = DataBlock.createEnbuDataObject(dtCompanyies2, fcCompanies2);
            edo2.titleString = "Title";
            edo2.SubTitles.Add("Summary of Report");
            edo2.showColumnHeaders = true;
            edo2.showPrevious = true;
            edo2.CalculatePreviousValues("CompanyID", "ID", "ID", true, true); //Calculate the previous values group data by CompanyID and sort by ID

            #endregion

            #region create a report object for multiple datatables 

            //Create Report
            SpreadSheet.EnbuReport eReport = new SpreadSheet.EnbuReport();
            String WorkSheetName = "WorkSheet1";
            eReport.CreateWorkSheet(WorkSheetName);

            eReport.AddDataObjectToWorkSheet(edo, WorkSheetName);
            eReport.AddDataObjectToWorkSheet(edo2, WorkSheetName);
            SpreadSheet.createGroupsBy(eReport, edo, "WorkSheet1", "CompanyID", "ID", "ID", "ID DESC", false, false);
            
            #endregion

            #region Output the result

            switch (OutputType)
            {
                case 1:
                    DataTableToHTML(edo); //Send Single Datatable to Excel
                    break;
                case 2:
                    DataTableToExcel(edo);
                    break;
                case 3:
                    ReportToHTML(eReport);
                    break;
                case 4:
                    ReportToExcel(eReport, true, 1, "C3");
                    break;
                case 5:
                    DataTableToHTML(edo2);
                    break;
                default:
                    
                    break;
            }

            #endregion
            
        }

        #endregion


        private void DataTableToHTML(DataBlock edo)
        {
            Response.Write(SpreadSheet.GetHTMLFromDataTable(edo));
        }

        private void DataTableToExcel(DataBlock edo)
        {
            //Export to Excel
            byte[] sSheet = SpreadSheet.ConvertDataTableToExcel(edo, "A1", "Worksheet1", true, "#B3B3B3", "#DEDEE7", "#FFFFFF", "#B8CCE4");
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=OppertunityProgress.xlsx");
            Response.BinaryWrite(sSheet);
            Response.End();
        }

        private void ReportToHTML(SpreadSheet.EnbuReport ebr)
        {
            Response.Write(ebr.ExportToHTML());
        }

        private void ReportToExcel(SpreadSheet.EnbuReport ebr, Boolean FormatOutput, int Spacing, String StartingCell = "A1")
        {
            byte[] sSheet = ebr.ExportToExcel(StartingCell, FormatOutput, Spacing);
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=OppertunityProgress.xlsx");
            Response.BinaryWrite(sSheet);
            Response.End();
        }

    }
}