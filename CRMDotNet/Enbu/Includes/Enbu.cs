﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eWare;

namespace CRMDotNet
{
    public class EnbuHelper
    {
        public eWareBase eWare;
        public Enbu.Includes.EnbuBaseFunctions EnbuBase;
        public Enbu.Includes.EnbuUIFunctions EnbuUI;
        public EnbuHelper()
        {
            CRMDotNet.eWare.eWareConnector eWareConn = new CRMDotNet.eWare.eWareConnector();
            eWare = eWareConn.Init();
            EnbuBase = new Enbu.Includes.EnbuBaseFunctions();
            EnbuUI = new Enbu.Includes.EnbuUIFunctions(eWare, EnbuBase, eWareConn);
        }
    }
}